// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDlyKqfr9zUDFin0uToGjRte3E2x4IhNrM",
    authDomain: "siep-c8a6e.firebaseapp.com",
    databaseURL: "https://siep-c8a6e.firebaseio.com",
    projectId: "siep-c8a6e",
    storageBucket: "siep-c8a6e.appspot.com",
    messagingSenderId: "632561590219",
    appId: "1:632561590219:web:47b69fc7e2c3e13e85604e",
    measurementId: "G-0DBD9FLCLM"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
