import { Component, OnInit } from '@angular/core';
import {ReportsService} from '../services/reports.service';
import { AreasDataService } from '../services/areas-data.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css']
})
export class HistorialComponent implements OnInit {
   //almacenan indices de seleccion de areas, tipos y departamentos
   index_area: number;
   index_tipo: number;
   index_departamento:number;
   index_pagination:number = 0;

   departamento:String;
   area;
   tipo;

   areas_array;
   tipos_array: String[];
   departamentos_array;

  public reportes: any;

  constructor(private reportsService: ReportsService, private areasDataService: AreasDataService, private titleService:Title) {
    titleService.setTitle("Historial de reportes - SIEP");
    this.getAreasData();
    this.getReportsDataList('','','',this.index_pagination);
    this.index_area=-1;
    this.index_tipo=-1
    this.index_departamento = -1;
    this.index_pagination=0;
    this.departamento = "";
    this.area = "";
    this.tipo = "";

   }

  ngOnInit(): void {

  }

  getReportsDataList(departamento,area,tipo, index){
    this.reportsService.getReportsDataList(departamento,area,tipo, index ).subscribe(result => {

      this.reportes = result;
    },
    error => {
      console.log(error);
    });
  }

  selectAreaChanged(data){
    this.index_area = data.target.value;
    this.index_tipo=-1;
    if(this.index_area!=-1){
      this.tipos_array =  this.areas_array[this.index_area].tipos;

    }
    else{
      this.tipos_array=[];
    }
  }
  selectDepartamentChanged(data){
    this.index_departamento = data.target.value;
    console.log(this.index_departamento);
  }
  selectTipoChanged(data){
    this.index_tipo=data.target.value;
    console.log(this.index_tipo);
    console.log(this.tipos_array[this.index_tipo]);

  }

  //Obtener datos para Seleccion de areas, tipos y departamentos
  getAreasData(){
    this.areasDataService.getAreasData().subscribe((result) => {
        this.areas_array = result;
    });
    this.areasDataService.getDepartamentosList().subscribe(
      result => {
        this.departamentos_array = result;
      }
    );
  }

  filterMarkers(){
    this.updateParametersToSearch();
    this.getReportsDataList(this.departamento, this.area, this.tipo, this.index_pagination);
  }


  goBackHistory(){
    this.index_pagination=this.index_pagination==0?this.index_pagination:this.index_pagination-1;
    this.getReportsDataList(this.departamento,this.area,this.tipo,this.index_pagination)
    console.log(this.index_pagination)
  }
  goFrontHistory(){

    if(this.reportes.length<10){
     return;

    }
    this.index_pagination++;
    console.log(this.index_pagination)
    this.getReportsDataList(this.departamento,this.area,this.tipo,this.index_pagination)

  }

  updateParametersToSearch(){
    console.log('departamento_i: '+this.index_departamento);
    console.log('area_i: '+this.index_area);
    console.log('tipo_i: '+this.index_tipo);
    console.log('indice'+this.index_pagination);
    this.departamento = this.index_departamento<0 ? '' : this.departamentos_array[this.index_departamento];
    this.area = this.index_area<0 ? '' : this.areas_array[this.index_area].area;
    this.tipo = this.index_tipo<0 ? '': this.tipos_array[this.index_tipo];
    console.log('departamento: '+this.departamento);
    console.log('area:'+this.area);
    console.log('tipo:'+this.tipo);


  }
}
