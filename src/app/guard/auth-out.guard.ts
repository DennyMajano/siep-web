import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthOutGuard implements CanActivate {

  constructor(private router: Router, private authService: AuthService){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      if(this.authService.isLoggedIn && this.authService.isAdmin){
        console.log("DIRIGIENDO A ADMIN");
        this.router.navigate(['/admin/usuarios']);
      }
      else if(this.authService.isLoggedIn && !this.authService.isAdmin){
        console.log("DIRIGIENDO A VISUALIZADOR");
        this.router.navigate(['/panel/general']);
      }
      console.log("NO LOGEADO");
      return true;

  }

}
