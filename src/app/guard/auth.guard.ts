import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService} from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(public authService: AuthService, public router: Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      console.log("ISLOGGED:"+this.authService.isLoggedIn);
      console.log("ISADMIN:"+this.authService.isAdmin);

    if(!this.authService.isLoggedIn || this.authService.isAdmin){
      console.log("NO ESTA LOGEADO COMO VISUALIZADOR");
      this.router.navigate(['/']);
    }
    console.log("ESTA LOGEADO COMO VISUALIZADOR");
      return true;
  }

}
