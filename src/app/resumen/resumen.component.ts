import { Component, OnInit } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { datos,multi } from '../data';
import {ReportsService} from '../services/reports.service';
import { Title } from '@angular/platform-browser';
import { title } from 'process';


@Component({
  selector: 'app-resumen',
  templateUrl: './resumen.component.html',
  styleUrls: ['./resumen.component.css']
})
export class ResumenComponent implements OnInit {
  datos;
  datosDepartments;
  datosAreas;
  datosChartDeparment;
  datosChartArea;

  multi: any[];

  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Departamentos';
  showYAxisLabel = true;
  yAxisLabel = 'Reportes';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  index_department: any;
  index_area:any;



  constructor( private reportsService: ReportsService, private titleServicio:Title) {
    //Object.assign(this, { datos });
    titleServicio.setTitle("Resumen de datos - SIEP");
    this.getDataCharts();
    this.getDataChartsDepartments();
    this.getDataChartsAreas();

  }

  ngOnInit(): void {
  }



  onSelect(event) {
    console.log(event);
  }


  getDataCharts(){
    this.reportsService.getDataCharts().subscribe(result => {
      console.log("__DATA");

      console.log(result);
      this.datos = result;
    });
  }

  getDataChartsDepartments(){
    this.reportsService.getDataChartsDepartments().subscribe(result => {

      this.datosDepartments = result;
    });
  }

  getDataChartsAreas(){
    this.reportsService.getDataChartsAreas().subscribe(result => {
      console.log("__DATA---------------------");

      console.log(result);
      this.datosAreas = result;
    });
  }

  selectDepartmentChanged(data){
    this.index_department = data.target.value;
    this.datosChartDeparment = this.index_department>-1?this.datosDepartments[this.index_department].value:"";

  }

  selectAreasChanged(data){
    this.index_area = data.target.value;
    this.datosChartArea = this.index_area>-1?this.datosAreas[this.index_area].value:"";

  }

}
