import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import {ReportsService} from '../services/reports.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-detalle-reporte',
  templateUrl: './detalle-reporte.component.html',
  styleUrls: ['./detalle-reporte.component.css']
})
export class DetalleReporteComponent implements OnInit {
  id;
  public data;
  area
  tipo
  public datos;


  constructor(private route: ActivatedRoute, private reportsService: ReportsService, private titleService: Title) {
    titleService.setTitle("Detalle reporte - SIEP");
    this.id = this.route.snapshot.paramMap.get('id');
    this.getReportData();

  }

  ngOnInit(): void {
    console.log("TRAYENDO DATOS2");
  }

  getReportData(){
    this.reportsService.getReportData(this.id).subscribe(
      result => {
        this.data = result;
        console.log('----')
        console.log(this.data)
        console.log(this.data.area)
      }
    );

  }

}
/*

*/
