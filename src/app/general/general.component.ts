import { Component, OnInit } from '@angular/core';
import {ReportsService} from '../services/reports.service';
import { AreasDataService } from '../services/areas-data.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.css']
})
export class GeneralComponent implements OnInit {
  //almacenan indices de seleccion de areas, tipos y departamentos
  index_area: number;
  index_tipo: number;
  index_departamento:number;

  areas_array;
  tipos_array: String[];
  departamentos_array;

  public console = console;

  //punto central del mapa ILOBASCO
  latitude: number = 13.8388596;
  longitude: number = -88.8718738;
  zoom:number= 9;
  markers: any;

  constructor( private reportsService: ReportsService, private areasDataService: AreasDataService, private titleService: Title) {
    this.getAreasData();
    this.getReportGeolocation('','','');

   }

   ngOnInit() {
    //this.setCurrentLocation();
    this.titleService.setTitle("General-SIEP");
    this.index_area=-1;
    this.index_tipo=-1
    this.index_departamento = -1;
  }


    //Obtener datos para mapa
    getReportGeolocation(departamento,area,tipo){
        this.reportsService.getReportToMap(departamento,area,tipo).subscribe((result) => {
          this.markers = result
            /*
          console.log("Primero");
          console.log(result);

          console.log('Segundo');
          console.log(result[0]);
          console.log('Tercero');
          console.log(result[0]['ubicacion']);
          console.log('Cuarto');
          console.log(result[0].ubicacion._longitude);
          console.log('quinto');
          console.log(result[0].ubicacion._latitude);
  */

        });
    }

    //Obtener datos para Seleccion de areas, tipos y departamentos
    getAreasData(){
      this.areasDataService.getAreasData().subscribe((result) => {
          this.areas_array = result;
      });
      this.areasDataService.getDepartamentosList().subscribe(
        result => {
          this.departamentos_array = result;
        }
      );
    }

    //Cambios en la seleccion de area, tipo, departamento
    selectAreaChanged(data){
      this.index_area = data.target.value;
      this.index_tipo=-1;
      if(this.index_area!=-1){
        this.tipos_array =  this.areas_array[this.index_area].tipos;

      }
      else{
        this.tipos_array=[];
      }
    }
    selectDepartamentChanged(data){
      this.index_departamento = data.target.value;
      console.log(this.index_departamento);
    }
    selectTipoChanged(data){
      this.index_tipo=data.target.value;
      console.log(this.index_tipo);
      console.log(this.tipos_array[this.index_tipo]);

    }
    //Realizar filtrador de datos
    filterMarkers(){
      const departamento = this.index_departamento<0 ? '' : this.departamentos_array[this.index_departamento];
      const area = this.index_area<0 ? '' : this.areas_array[this.index_area].area;
      const tipo = this.index_tipo<0 ? '': this.tipos_array[this.index_tipo];
      console.log('departamento = ' + departamento);
      console.log('area = ' + area);
      console.log('tipo = ' + tipo);
     this.getReportGeolocation(departamento, area, tipo);
    }


}
