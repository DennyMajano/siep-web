import { TestBed } from '@angular/core/testing';

import { AreasDataService } from './areas-data.service';

describe('AreasDataService', () => {
  let service: AreasDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AreasDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
