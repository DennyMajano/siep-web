import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Config} from '../../environments/config';

@Injectable({
  providedIn: 'root'
})
export class AreasDataService {

  constructor(private http: HttpClient) { }

  getAreasData(){
    return this.http.get(Config.API_BASE_URL+'/getAreasData');
  }

  getDepartamentosList(){
    return this.http.get(Config.API_BASE_URL+'/getDepartmentData');
  }

}
