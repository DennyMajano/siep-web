import { Injectable, NgZone } from '@angular/core';
import { Router } from  "@angular/router";
import { AngularFireAuth } from  "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { User, firestore } from  'firebase';
import {Config} from '../../environments/config';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class AuthService {
  user;
  userChanges: BehaviorSubject<any> = new BehaviorSubject<any>(this.user);

  constructor(private http: HttpClient,public  afAuth:  AngularFireAuth, public  router:  Router, public aFire: AngularFirestore) {
/*
    this.afAuth.authState.subscribe(user=>{
      if(user){
        const userDoc = this.aFire.doc("usuariosweb/"+user.email).ref.get();
        userDoc.then(
          doc => {
           if(doc.exists){
            this.setUserChange(doc.data());
            sessionStorage.setItem("rol",this.user.rol.admin);
           }
           else{
             console.log("No  hay datos");
           }
          }
        ).catch(
          err => console.log(err)
        );

      }
    });
*/
  }

  setUserChange(user){
    this.user=user;
    this.userChanges.next(user);
  }

login(email: string, password: string) {
    return this.afAuth.signInWithEmailAndPassword(email, password).then(

      data =>{
        //Si es valido buscamos en Firebase los datos
        const userDoc = this.aFire.doc("usuariosweb/"+data.user.email).ref.get();
        userDoc.then(
          doc => {
           if(doc.exists){
            console.log(doc.data());
            this.setUserChange(doc.data());
            localStorage.setItem("rol",this.user.rol.admin);
            if(this.user.rol.admin){
              this.router.navigate(["/admin/usuarios"]);
            }
            else{
              this.router.navigate(["/panel/general"]);
            }
           }
           else{
             console.log("No  hay datos");
           }
          }
        ).catch(
          err => console.log(err)
        );

      }

      ).catch((error)=>{
        window.alert(error);
    })

}

async logout(){
  this.afAuth.signOut().then(()=>{
    localStorage.removeItem("rol");

    window.location.reload();
    //this.router.navigate(['/']);
  }).catch((error)=>{
    window.alert(error);
  });

}


get isLoggedIn(): boolean {
  const rol = localStorage.getItem("rol");
  console.log("USER??\n"+rol);
  return rol!==null;
}

get isAdmin(): boolean {
  const  rol  =  JSON.parse(localStorage.getItem("rol"));
  console.log("ROL="+rol);
  return  rol;
}


userUpdate(){
  this.afAuth.onAuthStateChanged(currentUser => {
    if(currentUser){
      const userDoc = this.aFire.doc("usuariosweb/"+currentUser.email).ref.get();
      userDoc.then(
        doc => {
         if(doc.exists){
          console.log("USUARIO CARGADO")
          console.log(doc.data())
          this.setUserChange(doc.data());
         }
         else{
           console.log("No  hay datos");
         }
        }
      ).catch(
        err => console.log(err)
      );
    }
  });
}



}
