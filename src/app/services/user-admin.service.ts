import { Injectable, NgZone } from '@angular/core';
import { Router } from  "@angular/router";
import { AngularFireAuth } from  "@angular/fire/auth";

import { HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Config} from '../../environments/config';




import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { FirebaseApp } from '@angular/fire';
import { throwError } from 'rxjs/internal/observable/throwError';


@Injectable({
  providedIn: 'root'
})
export class UserAdminService {


  constructor(private http: HttpClient,private aFirestore: AngularFirestore,public  afAuth:  AngularFireAuth, public  router:  Router, private firebase: FirebaseApp) {
  }

  async register(email: string, password: string, nombre: string, apellido:string) {



    this.afAuth.createUserWithEmailAndPassword(email, password).then((user)=>{
      user.user.updateProfile(
       {displayName: nombre}
     ).then(()=>{

      this.aFirestore.collection('usuariosweb').doc(email).set({
        rol: {admin: false},
        "nombre": nombre,
        "apellidos": apellido,
        "email": email,
        "uid": user.user.uid
      }).then(()=>{

        this.sendEmailVerification();

      }).catch(error=>{
        console.log(error);
      });

     }).catch(error =>{
      console.log(error);
     });

    }).catch((error)=>{
     window.alert(error);
    });


   }

   async sendEmailVerification() {
      (await this.afAuth.currentUser).sendEmailVerification();
    }



  getListUser(){
    return this.http.get(Config.API_BASE_URL+"/getUserList");
  }

  disableUser(uid){
     return this.http.get(Config.API_BASE_URL+"/disableUser?id="+uid)

  }

  enableUser(uid){
    console.log("llamado");
    return this.http.get(Config.API_BASE_URL+"/enableUser?id="+uid);
  }




  resetPasswordEmail(email: string){
   this.afAuth.sendPasswordResetEmail(email)
    .then(() => {
      window.alert("Correo para renovar contraseña enviado");
    })
    .catch(
      error => {

        window.alert("Un error ha ocurrido al enviar correo de renovación de contraseña:\n"+error);
      }
    );
  }




    // List batch of users, 1000 at a time.
   /* admin.auth().listUsers(10)
      .then(function(listUsersResult) {

        listUsersResult.users.forEach(function(userRecord) {
          console.log('user', userRecord.toJSON());
        });

      })
      .catch(function(error) {
        console.log('Error listing users:', error);
      });

*/


private handleError(error: HttpErrorResponse) {
  if (error.error instanceof ErrorEvent) {
    // A client-side or network error occurred. Handle it accordingly.
    console.error('An error occurred:', error.error.message);
  } else {
    // The backend returned an unsuccessful response code.
    // The response body may contain clues as to what went wrong,
    console.error(
      `Backend returned code ${error.status}, ` +
      `body was: ${error.error}`);
  }
  // return an observable with a user-facing error message
  return throwError(
    'Something bad happened; please try again later.');
};
}



