import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Config} from '../../environments/config';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  constructor(private http: HttpClient) { }

  getReports(){
    return this.http.get(Config.API_BASE_URL+"/getreports");
  }

  getReportToMap(departamento: String, area: String, tipo: String){
    return this.http.get(Config.API_BASE_URL+"/getreporttomap?departamento="+departamento+"&area="+area+"&tipo="+tipo);
  }

  getReportData(id:String){
    return this.http.get(Config.API_BASE_URL+"/getreportData?id="+id)
  }

  getReportsDataList(departamento: String, area: String, tipo: String, index:number){
    return this.http.get(Config.API_BASE_URL+"/getreportsDataList?departamento="+departamento+"&area="+area+"&tipo="+tipo+"&index="+index);
  }

  getDataCharts(){
    return this.http.get(Config.API_BASE_URL+"/getChartsData");
  }
  getDataChartsDepartments(){
    return this.http.get(Config.API_BASE_URL+"/getChartsDataDepartaments");
  }
  getDataChartsAreas(){
    return this.http.get(Config.API_BASE_URL+"/getChartsDataAreas");
  }
}
