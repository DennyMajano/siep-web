import { Injectable, NgZone } from '@angular/core';
import { Router } from  "@angular/router";
import { AngularFireAuth } from  "@angular/fire/auth";

import { User } from  'firebase';

@Injectable({
  providedIn: 'root'
})
export class AdminAuthService {
  user: User;
  constructor(public  afAuth:  AngularFireAuth, public  router:  Router) {
    this.afAuth.authState.subscribe(user => {
      if (user){
        this.user = user;
        localStorage.setItem('user', JSON.stringify(this.user));

      } else {
        localStorage.setItem('user', null);
        localStorage.setItem('rol', null);

      }
    })
  }

  async login(email: string, password: string) {
    this.afAuth.signInWithEmailAndPassword(email, password).then(()=>{
      localStorage.setItem('rol', "super");
      this.router.navigate(['/admin/usuarios']);
    }).catch((error)=>{
        window.alert(error);
    })

}

async logout(){
  this.afAuth.signOut().then(()=>{
    localStorage.removeItem('user');
    localStorage.removeItem('rol');
    this.router.navigate(['/']);
  }).catch((error)=>{
    window.alert(error);
  });

}

get isLoggedIn(): boolean {
  const  user  =  JSON.parse(localStorage.getItem('user'));
  return  user  !==  null;
}

}
