import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import { Router } from  "@angular/router";

@Component({
  selector: 'app-login-user',
  templateUrl: './login-user.component.html',
  styleUrls: ['./login-user.component.css']
})
export class LoginUserComponent implements OnInit {
  public password: string;
  public email: string;

  constructor(public authService: AuthService, public  router:  Router) {

   }

  ngOnInit(): void {
  }

  onLogin(){
    this.authService.login(this.email,this.password);
  }

}
