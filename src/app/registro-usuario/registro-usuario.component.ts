import { Component, OnInit } from '@angular/core';
import { UserAdminService } from '../services/user-admin.service';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-registro-usuario',
  templateUrl: './registro-usuario.component.html',
  styleUrls: ['./registro-usuario.component.css']
})
export class RegistroUsuarioComponent implements OnInit {
  usuariosList;

  constructor(public userAdmin: UserAdminService, private router: Router,private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle("Nuevo usuario - SIEP");
  }



  verificarDatosDeRegistro(pass1: string, pass2: string, nombre: string, apellidos: string, mail: string){
      if(pass1===pass2 && pass1.length>=6){
        this.userAdmin.register(mail,pass1, nombre, apellidos).then(()=>{
          this.router.navigate(["/admin/usuarios"]);
        }).catch(error=>{
          window.alert(error);
        });
      }
      else{
        window.alert("Contraseñas no coinciden");
      }
  }




}
