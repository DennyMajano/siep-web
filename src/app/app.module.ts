import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {AgmCoreModule} from '@agm/core';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { NgxChartsModule } from '@swimlane/ngx-charts';



import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginUserComponent } from './login-user/login-user.component';
import { LoginAdminComponent } from './login-admin/login-admin.component';
import { DashboardMenuComponent } from './dashboard-menu/dashboard-menu.component';
import { GeneralComponent } from './general/general.component';
import { ResumenComponent } from './resumen/resumen.component';
import { HistorialComponent } from './historial/historial.component';
import { DetalleReporteComponent } from './detalle-reporte/detalle-reporte.component';
import { DashboardMenuAdminComponent } from './dashboard-menu-admin/dashboard-menu-admin.component';
import { UsuariosListComponent } from './usuarios-list/usuarios-list.component';
import { RegistroUsuarioComponent } from './registro-usuario/registro-usuario.component';
import { EdicionUsuarioComponent } from './edicion-usuario/edicion-usuario.component';

//Servicio de autenticaciòn
import {AuthService} from './services/auth.service';
import {ReportsService} from './services/reports.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginUserComponent,
    LoginAdminComponent,
    DashboardMenuComponent,
    GeneralComponent,
    ResumenComponent,
    HistorialComponent,
    DetalleReporteComponent,
    DashboardMenuAdminComponent,
    UsuariosListComponent,
    RegistroUsuarioComponent,
    EdicionUsuarioComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAtb1SST14klrrOWjjTuV3aTgCqw0FuX6Q',
      libraries: ['places']
    }),
    AgmJsMarkerClustererModule,
    NgxChartsModule,
  ],
  providers: [AuthService, ReportsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
