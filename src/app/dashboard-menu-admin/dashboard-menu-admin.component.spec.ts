import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardMenuAdminComponent } from './dashboard-menu-admin.component';

describe('DashboardMenuAdminComponent', () => {
  let component: DashboardMenuAdminComponent;
  let fixture: ComponentFixture<DashboardMenuAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardMenuAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardMenuAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
