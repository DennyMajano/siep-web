import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'app-dashboard-menu-admin',
  templateUrl: './dashboard-menu-admin.component.html',
  styleUrls: ['./dashboard-menu-admin.component.css']
})
export class DashboardMenuAdminComponent implements OnInit {

  constructor(public adminAuthService: AuthService) { }

  ngOnInit(): void {
  }

}
