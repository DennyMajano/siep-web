import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginUserComponent } from './login-user/login-user.component';
import { LoginAdminComponent } from './login-admin/login-admin.component';
import { DashboardMenuComponent } from './dashboard-menu/dashboard-menu.component';
import { GeneralComponent } from './general/general.component';
import { ResumenComponent} from './resumen/resumen.component';
import { HistorialComponent} from './historial/historial.component';
import {DetalleReporteComponent} from './detalle-reporte/detalle-reporte.component';
import {DashboardMenuAdminComponent} from './dashboard-menu-admin/dashboard-menu-admin.component';
import {UsuariosListComponent} from './usuarios-list/usuarios-list.component';
import {RegistroUsuarioComponent} from './registro-usuario/registro-usuario.component';
import {EdicionUsuarioComponent} from './edicion-usuario/edicion-usuario.component';

import {AuthGuard} from './guard/auth.guard';
import {AuthOutGuard} from './guard/auth-out.guard';
import {AdminGuardGuard} from './guard/admin-guard.guard';

const routes: Routes = [
  {path: "", component: LoginUserComponent, canActivate: [AuthOutGuard]},
  {path: "login",component: LoginUserComponent, canActivate: [AuthOutGuard]},
  {path: "panel",component: DashboardMenuComponent,canActivate: [AuthGuard],
    children:[
      {path: "general",component: GeneralComponent},
      {path: "resumen",component: ResumenComponent},
      {path: "historial", component: HistorialComponent},
      {path: "detallereporte/:id",component: DetalleReporteComponent}

    ]
  },
  {path: "admin",component: DashboardMenuAdminComponent, canActivate: [AdminGuardGuard],
    children:[
      {path: "usuarios",component: UsuariosListComponent},
      {path: "registrarusuario",component: RegistroUsuarioComponent},
      {path: "editarusuario", component: EdicionUsuarioComponent}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
