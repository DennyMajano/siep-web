import { Component, OnInit } from '@angular/core';
import { UserAdminService } from '../services/user-admin.service';
import { Router } from  "@angular/router";
import { Title } from '@angular/platform-browser';
import { title } from 'process';

@Component({
  selector: 'app-usuarios-list',
  templateUrl: './usuarios-list.component.html',
  styleUrls: ['./usuarios-list.component.css']
})
export class UsuariosListComponent implements OnInit {
  usuariosList;
  constructor(public userAdmin: UserAdminService,public  router:  Router, private titleService: Title) {
    this.getUserList();
   }

  ngOnInit(): void {
    this.titleService.setTitle("Administrar usuarios - SIEP");
  }


  getUserList(){
    this.userAdmin.getListUser().subscribe(result => {
        this.usuariosList = result;
    });
  }


  sendResetPasswordEmail(email){
    this.userAdmin.resetPasswordEmail(email);
  }

  enableUser(uid){
    console.log("habilitar");
    this.userAdmin.enableUser(uid).subscribe(
      result=>{
        this.getUserList();
      }
    );
  }

  disableUser(uid){
    this.userAdmin.disableUser(uid).subscribe(
      result=>{
        this.getUserList();
      }
    );
  }


}
