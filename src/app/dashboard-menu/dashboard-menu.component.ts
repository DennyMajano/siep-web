import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'app-dashboard-menu',
  templateUrl: './dashboard-menu.component.html',
  styleUrls: ['./dashboard-menu.component.css']
})
export class DashboardMenuComponent implements OnInit {

  userData = this.authService.user;
  constructor(public authService: AuthService) { }

  ngOnInit(): void {
    //this.authService.userChanges.subscribe(user => this.userData=user);
    console.log("FIRST");
    console.log(this.userData);

    this.authService.userUpdate();
    this.authService.userChanges.subscribe(data=>{

      console.log("SUSCRIPCION");
      this.userData=data});
    console.log("AFTER");
    console.log(this.userData);

  }


}
